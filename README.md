Before run.

In project directory:
- yarn install


IMPORTANT !  -- required to run App on iOS Simulator device:
- cd ios   
- pod install   


Prerequisites:
- Node.js and npm (installing Node.js also installs npm)
- Yarn (recommended)
- git
- react-native-cli - $ npm install -g react-native-cli
- OSX - > Cocoapods - $ gem install cocoapods


Scripts: Npm scripts in package, run as yarn {command}

- start - react-native Metro Bundler
- ios  - builds your app and starts it on iOS simulator
- android - builds your app and starts it on Android simulator/device
