import React from 'react';
import {StyleSheet} from 'react-native';
import {Router, Scene, Stack} from 'react-native-router-flux';
import CategoryList from './Screens/CategoryList';
import Home from './Screens/Home';
import ArticleScreen from './Screens/ArticleScreen';
import RecipeScreen from './Screens/RecipeScreen';

const AppRouter = () => {
  return (
    <Router sceneStyle={styles.root}>
      <Stack key="root">
        <Scene key="Home" title="Welcome To Recipe App" component={Home} />
        <Scene
          key="CategoryList"
          title="Main Categories"
          component={CategoryList}
        />
        <Scene key="Article" title="Article" component={ArticleScreen} />
        <Scene key="Recipe" title="Recipe" component={RecipeScreen} />
      </Stack>
    </Router>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});
export default AppRouter;
