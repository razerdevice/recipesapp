import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity as Touchable,
  Text,
  Image,
} from 'react-native';
import Trash from '../../assets/delete.png';
import Edit from '../../assets/pencil.png';

const ListItem = ({_id, title, text, onPress, remove, onEdit}) => {
  return (
    <Touchable onPress={() => onPress(_id, title)} style={styles.container}>
      <View style={{flex: 4 / 5}}>
        <Text>{title}</Text>
        <Text>{text}</Text>
      </View>
      <Touchable onPress={() => !!onEdit && onEdit(_id)} style={styles.action}>
        <Image source={Edit} />
      </Touchable>
      <Touchable onPress={() => remove(_id)} style={styles.action}>
        <Image source={Trash} />
      </Touchable>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 55,
    padding: 10,
  },
  action: {
    flex: 1 / 8,
    alignItems: 'flex-end',
  },
});

export default ListItem;
