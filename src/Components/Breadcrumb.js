import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const Breadcrumb = ({history}) => {
  const routes = history.map(route => route.params.title).filter(i => !!i);
  return (
    <View style={styles.breadcrumb}>
      <Text multiline={true}>
        {routes.map((route, index) => {
          const isLast = routes.length === index + 1;
          return (
            <Text
              key={index.toString()}
              style={styles.breadcrumbText}
              numberOfLines={4}>
              {isLast ? route : `${route} > `}
            </Text>
          );
        })}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  breadcrumb: {
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 7,
  },
  breadcrumbText: {
    color: '#1FAAD4',
  },
});

export default Breadcrumb;
