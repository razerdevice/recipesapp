import React, {useState, useEffect, createRef} from 'react';
import {Text, View, StyleSheet, Button, TextInput} from 'react-native';
import Modal from 'react-native-modal';
import Toast from 'react-native-easy-toast';

const MyModal = ({
  val,
  _description,
  _text,
  title = '',
  onHide,
  isVisible,
  onSubmit,
  type,
}) => {
  useEffect(() => {
    setValue(val);
    setText(_text);
    setDescription(_description);
  }, [val, _text, _description]);

  const [value, setValue] = useState(val);
  const [text, setText] = useState(_text);
  const [description, setDescription] = useState(_description);

  const toastRef = createRef();

  const validate = () => {
    if (type === 'category' && value) {
      onSubmit({value, text, description});
      return setValue('');
    }
    if (type === 'article' && value && text && description) {
      onSubmit({value, text, description});
      setText('');
      setDescription('');
      return setValue('');
    }
    if (type === 'recipe' && value && text) {
      onSubmit({value, text, description});
      setText('');
      return setValue('');
    }
    toastRef.current.show('Enter all values value!');
  };

  return (
    <Modal isVisible={isVisible}>
      <View style={styles.container}>
        <Text style={styles.title}>Add new item</Text>
        <TextInput
          value={value}
          placeholder="Title"
          onChangeText={t => setValue(t)}
          style={styles.input}
        />
        {type !== 'category' && (
          <TextInput
            value={text}
            placeholder="Text"
            onChangeText={t => setText(t)}
            style={styles.input}
          />
        )}
        {type === 'article' && (
          <TextInput
            value={description}
            placeholder="Description"
            onChangeText={t => setDescription(t)}
            style={styles.input}
          />
        )}
        <View style={styles.footer}>
          <Button title="Cancel" onPress={onHide} />
          <Button title="Submit" onPress={() => validate()} />
        </View>
      </View>
      <Toast ref={toastRef} position="top" />
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
    width: '100%',
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'space-between',
    borderRadius: 15,
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
  },
  input: {
    marginBottom: 10,
    borderBottomWidth: 1,
    borderColor: '#708090',
    fontSize: 16,
  },
  footer: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default MyModal;
