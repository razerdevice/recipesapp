import React, {Component} from 'react';
import {observer, Provider} from 'mobx-react';
import stores from './Stores/rootStore';
import AppRouter from './AppRouter';

@observer
class App extends Component {
  render() {
    return (
      <Provider {...stores}>
        <AppRouter />
      </Provider>
    );
  }
}

export default App;
