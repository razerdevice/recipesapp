import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {observer} from 'mobx-react';
import {Actions} from 'react-native-router-flux';
import Breadcrumb from '../Components/Breadcrumb';
@observer
class ArticleScreen extends Component {
  render() {
    const {title, text} = this.props.article;
    return (
      <View style={styles.scene}>
        <Breadcrumb history={Actions.prevState.routes} />
        <View style={styles.container}>
          <Text style={styles.title} ellipsizeMode="tail" numberOfLines={1}>
            {title}
          </Text>
          <Text multiline={true}>{text}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    backgroundColor: '#E8E8E8',
  },
  container: {
    marginTop: 10,
    backgroundColor: 'white',
    flex: 1,
    padding: 10,
  },
  title: {
    textAlign: 'center',
    fontSize: 24,
    color: '#5C5C5C',
    marginVertical: 10,
    fontWeight: '600',
  },
  text: {
    fontSize: 16,
    color: '#5C5C5C',
  },
  breadcrumb: {
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 10,
  },
  breadcrumbText: {
    color: '#1FAAD4',
  },
});

export default ArticleScreen;
