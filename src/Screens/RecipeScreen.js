import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {observer, inject} from 'mobx-react';
import Breadcrumb from '../Components/Breadcrumb';
import {Actions} from 'react-native-router-flux';

@inject('recipeStore')
@observer
class RecipeScreen extends Component {
  render() {
    const {title, text} = this.props.recipe;
    return (
      <View style={styles.scene}>
        <Breadcrumb history={Actions.prevState.routes} />
        <View style={styles.container}>
          <Text style={styles.title} ellipsizeMode="tail" numberOfLines={1}>
            {title}
          </Text>
          <Text multiline={true}>{text}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    backgroundColor: '#E8E8E8',
  },
  container: {
    marginTop: 10,
    backgroundColor: 'white',
    flex: 1,
    padding: 10,
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    color: '#5C5C5C',
    marginVertical: 10,
    fontWeight: '600',
  },
  text: {
    fontSize: 16,
    color: '#5C5C5C',
  },
  breadcrumb: {
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 10,
  },
  breadcrumbText: {
    color: '#1FAAD4',
  },
});

export default RecipeScreen;
