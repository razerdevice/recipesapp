import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import {observer, inject} from 'mobx-react';
import {Actions} from 'react-native-router-flux';
import Add from '../../assets/plus-circle.png';
import ListItem from '../Components/ListItem';
import {SpecificObject} from '../Stores/specificObject';

@inject('recipeStore')
@observer
class RecipeTab extends Component {
  componentDidMount() {
    const store: SpecificObject = this.props.recipeStore;
    const {id} = this.props;
    store.fetchByCategory(id);
  }
  renderSeparator = () => <View style={styles.separator} />;

  onRemove = id => {
    const store: SpecificObject = this.props.recipeStore;
    store.remove(id);
  };

  render() {
    const recipeStore: SpecificObject = this.props.recipeStore;
    const {id, onEdit, toggleModal} = this.props;
    const recipes = recipeStore.items.filter(item => item.categoryId === id);
    return (
      <View style={[styles.scene, {backgroundColor: '#F7EFE8'}]}>
        <FlatList
          data={recipes}
          ItemSeparatorComponent={this.renderSeparator}
          renderItem={({item}) => (
            <ListItem
              {...item}
              onPress={() => Actions.Recipe({recipe: item})}
              onEdit={() => onEdit(item, 'recipe')}
              remove={this.onRemove}
            />
          )}
          keyExtractor={({_id}) => _id.toString()}
        />
        <TouchableOpacity
          style={styles.addItem}
          onPress={() => toggleModal('recipe')}>
          <Image source={Add} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 55,
    padding: 10,
    borderWidth: 1,
  },
  action: {
    flex: 1 / 8,
    alignItems: 'flex-end',
  },
  addItem: {
    position: 'absolute',
    bottom: 30,
    right: 30,
  },
});

export default RecipeTab;
