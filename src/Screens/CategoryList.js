import React, {Component, Fragment} from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';
import {observer, inject} from 'mobx-react';
import {GenericObject} from '../Stores/genericObject';
import {Actions} from 'react-native-router-flux';
import Modal from '../Components/Modal';
import {TabView, SceneMap} from 'react-native-tab-view';
import RecipeTab from './RecipeTab';
import ArticleTab from './ArticleTab';
import CategoryTab from './CategoryTab';
import Breadcrumb from '../Components/Breadcrumb';

const {width} = Dimensions.get('window');

@inject('categoryStore')
@inject('articleStore')
@inject('recipeStore')
@observer
class Home extends Component {
  state = {
    isVisibleModal: false,
    currentEdit: null,
    index: 0,
    history: [],
    routes: [
      {key: 'first', title: 'Categories'},
      {key: 'second', title: 'Recipes'},
      {key: 'third', title: 'Articles'},
    ],
    type: null,
  };

  componentDidMount() {
    const {id} = this.props;
    if (id) {
      const history = Actions.prevState.routes;
      this.setState({history});
    }
  }

  componentWillUnmount() {
    const store: GenericObject = this.props.categoryStore;
    const {parentId} = this.props;
    store.setId(parentId);
  }

  onItemPress = (id, title) => {
    const store: GenericObject = this.props.categoryStore;
    const {parentId} = this.props;
    store.setId(id, parentId);
    Actions.CategoryList({id, parentId: this.props.id, title});
  };

  onEdit = (item, type) => {
    this.setState({currentEdit: item, isVisibleModal: true, type});
  };

  onSubmitEdit = async ({value, text, description}) => {
    const {id, categoryStore, recipeStore, articleStore} = this.props;
    const {type} = this.state;
    let store;
    switch (type) {
      case 'recipe':
        store = recipeStore;
        break;
      case 'article':
        store = articleStore;
        break;
      case 'category':
        store = categoryStore;
        break;
    }
    const {currentEdit} = this.state;
    if (currentEdit) {
      await store.update({...currentEdit, title: value, text, description});
    } else {
      await store.create({
        title: value,
        text,
        description,
        categoryId: id,
        parentId: id,
      });
    }
    this.toggleModal();
  };

  toggleModal = type => {
    const {isVisibleModal} = this.state;
    this.setState({isVisibleModal: !isVisibleModal, currentEdit: null, type});
  };

  render() {
    const {currentEdit, history} = this.state;
    const {id} = this.props;
    return (
      <View style={styles.container}>
        {id ? (
          <Fragment>
            <Breadcrumb history={history} />
            <TabView
              lazy
              navigationState={this.state}
              renderScene={SceneMap({
                first: () => (
                  <CategoryTab
                    id={id}
                    toggleModal={this.toggleModal}
                    onItemPress={this.onItemPress}
                    onEdit={this.onEdit}
                  />
                ),
                second: () => (
                  <RecipeTab
                    id={id}
                    toggleModal={this.toggleModal}
                    onEdit={this.onEdit}
                  />
                ),
                third: () => (
                  <ArticleTab
                    id={id}
                    toggleModal={this.toggleModal}
                    onEdit={this.onEdit}
                  />
                ),
              })}
              onIndexChange={index => this.setState({index})}
              initialLayout={{width}}
            />
          </Fragment>
        ) : (
          <CategoryTab
            id={id}
            onRemove={this.onRemove}
            toggleModal={this.toggleModal}
            onItemPress={this.onItemPress}
            onEdit={this.onEdit}
          />
        )}
        <Modal
          isVisible={this.state.isVisibleModal}
          onHide={this.toggleModal}
          onSubmit={this.onSubmitEdit}
          placeholder="Add Category"
          type={this.state.type}
          val={currentEdit ? currentEdit.title : ''}
          _description={currentEdit ? currentEdit.description : ''}
          _text={currentEdit ? currentEdit.text : ''}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  scene: {
    flex: 1,
  },

});

export default Home;
