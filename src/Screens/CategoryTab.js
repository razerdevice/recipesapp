import {inject, observer} from 'mobx-react';
import React, {Component} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import Add from '../../assets/plus-circle.png';
import ListItem from '../Components/ListItem';
import {GenericObject} from '../Stores/genericObject';
import {SpecificObject} from '../Stores/specificObject';

@inject('categoryStore')
@observer
class Category extends Component {
  renderSeparator = () => <View style={styles.separator} />;

  onRemove = id => {
    const store: SpecificObject = this.props.categoryStore;
    store.remove(id);
  };

  render() {
    const {id, toggleModal, onItemPress, onEdit} = this.props;
    const categoryStore: GenericObject = this.props.categoryStore;
    const {rootItems, itemsList} = categoryStore;
    return (
      <>
        <FlatList
          data={id ? itemsList : rootItems}
          ItemSeparatorComponent={this.renderSeparator}
          renderItem={({item}) => (
            <ListItem
              {...item}
              onPress={onItemPress}
              onEdit={() => onEdit(item, 'category')}
              remove={this.onRemove}
            />
          )}
          keyExtractor={({_id}) => _id.toString()}
        />
        <TouchableOpacity
          style={styles.addItem}
          onPress={() => toggleModal('category')}>
          <Image source={Add} />
        </TouchableOpacity>
      </>
    );
  }
}

const styles = StyleSheet.create({
  addItem: {
    position: 'absolute',
    bottom: 30,
    right: 30,
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#D3D3D3',
  },
});

export default Category;
