import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {observer, inject} from 'mobx-react';
import {Actions} from 'react-native-router-flux';

@inject('categoryStore')
@observer
class Home extends Component {
  componentDidMount() {
    const {categoryStore} = this.props;
    categoryStore.fetchAll();
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.container}>
          <TouchableOpacity onPress={() => Actions.CategoryList()}>
            <Text style={styles.text}>To Category List</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'white',
  },
  text: {
    fontSize: 20,
    borderBottomWidth: 1,
    lineHeight: 35,
  },
});

export default Home;
