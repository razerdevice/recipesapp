import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import {observer, inject} from 'mobx-react';
import Add from '../../assets/plus-circle.png';
import ListItem from '../Components/ListItem';
import {SpecificObject} from '../Stores/specificObject';
import {Actions} from 'react-native-router-flux';

@inject('articleStore')
@observer
class ArticleTab extends Component {
  componentDidMount() {
    const store: SpecificObject = this.props.articleStore;
    const {id} = this.props;
    store.fetchByCategory(id);
  }

  renderSeparator = () => <View style={styles.separator} />;

  onRemove = id => {
    const store: SpecificObject = this.props.articleStore;
    store.remove(id);
  };

  render() {
    const store: SpecificObject = this.props.articleStore;
    const {id, onEdit, toggleModal} = this.props;
    const articles = store.items.filter(item => item.categoryId === id);
    return (
      <View style={styles.scene}>
        <FlatList
          data={articles}
          ItemSeparatorComponent={this.renderSeparator}
          renderItem={({item}) => (
            <ListItem
              {...item}
              onPress={() => Actions.Article({article: item})}
              onEdit={() => onEdit(item, 'article')}
              remove={this.onRemove}
            />
          )}
          keyExtractor={({_id}) => _id.toString()}
        />
        <TouchableOpacity
          style={styles.addItem}
          onPress={() => toggleModal('article')}>
          <Image source={Add} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    backgroundColor: '#E6F4F5',
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#D3D3D3',
  },
  addItem: {
    position: 'absolute',
    bottom: 30,
    right: 30,
  },
});

export default ArticleTab;
