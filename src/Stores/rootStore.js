import GenericObject from './genericObject';
import SpecificObject from './specificObject';

export default {
  categoryStore: new GenericObject('category'),
  articleStore: new SpecificObject('article'),
  recipeStore: new SpecificObject('recipe'),
};
