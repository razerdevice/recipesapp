import {observable, action, runInAction, computed} from 'mobx/lib/mobx';
import API from '../API';
import _ from 'lodash';

export class GenericObject {
  constructor(objectType) {
    this.objectType = objectType;
  }

  @observable.shallow items = [];
  @observable isFetching = false;
  @observable error = null;
  @observable parentId = null;
  @observable id = null;

  @computed
  get rootItems() {
    return this.items.filter(({parentId}) => _.isNil(parentId));
  }

  @computed
  get itemsList() {
    return this.items.filter(({parentId}) => parentId === this.id);
  }

  @action setId = (id, parentId) => {
    this.id = id;
    this.parentId = parentId;
  };

  @action async create(payload) {
    try {
      const {data} = await API.create(this.objectType, payload);
      runInAction(() => (this.items = [data, ...this.items]));
      return true;
    } catch (e) {
      console.log(e);
    }
  }

  @action async fetchAll() {
    try {
      const {data} = await API.fetchAll(this.objectType);
      runInAction(() => (this.items = [...data]));
    } catch (e) {
      console.log(e);
    }
  }

  @action async fetchBreadcrumbs(id) {
    try {
      const {data} = await API.fetchBreadcrumbs(this.objectType, id);
    } catch (e) {
      console.log(e);
    }
  }

  @action async remove(id) {
    try {
      const {data} = await API.remove(this.objectType, id);
      runInAction(() => {
        this.items = this.items.filter(i => i._id !== id);
      });
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  }

  @action async update(payload) {
    try {
      await API.update(this.objectType, payload);
      const updated = this.items.map(i => {
        if (i._id === payload._id) {
          return payload;
        }
        return i;
      });
      runInAction(() => (this.items = [...updated]));
    } catch (e) {
      console.log(e);
    }
  }
}

export default GenericObject;
