import {action, runInAction} from 'mobx/lib/mobx';
import {GenericObject} from './genericObject';
import API from '../API';

export class SpecificObject extends GenericObject {
  constructor(objectType) {
    super();
    this.objectType = objectType;
  }
  categoryId = null;
  cachedCategories = [];

  @action async fetchById(id) {
    try {
      const {data} = await API.fetchById(this.objectType, id);
      console.log(data);
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  }

  @action async fetchByCategory(categoryId) {
    if (this.cachedCategories.includes(categoryId)) {
      return runInAction(() => (this.categoryId = categoryId));
    }
    try {
      const {data} = await API.fetchByCategory(this.objectType, categoryId);
      runInAction(() => {
        this.items = [...this.items, ...data];
        this.cachedCategories.push(categoryId);
        this.categoryId = categoryId;
      });
    } catch (e) {
      console.log(e);
    }
  }
}

export default SpecificObject;
