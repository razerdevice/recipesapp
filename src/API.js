import axios from 'axios';
import {API_BASE} from './Constans';

const fetchById = (objectType, id) => {
  return axios.get(`${API_BASE}/${objectType}/item/${id}`);
};

const fetchAll = objectType => {
  return axios.get(`${API_BASE}/${objectType}/all`);
};

const fetchBreadcrumbs = (objectType, id) => {
  return axios.get(`${API_BASE}/${objectType}/categoryList/${id}`);
};

const remove = (objectType, id) => {
  return axios.delete(`${API_BASE}/${objectType}/${id}`);
};

const update = (objectType, payload) => {
  return axios.put(`${API_BASE}/${objectType}/update`, payload);
};

const fetchByCategory = (objectType, categoryId) => {
  return axios.get(`${API_BASE}/${objectType}/byCategory/${categoryId}`);
};

const create = (objectType, payload) => {
  return axios.post(`${API_BASE}/${objectType}/create`, payload);
};

export default {
  fetchAll,
  fetchById,
  fetchBreadcrumbs,
  remove,
  update,
  fetchByCategory,
  create,
};
